import { Component, viewChild } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { Dropdown, DropdownChangeEvent, DropdownModule } from 'primeng/dropdown';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    CalendarModule,
    FormsModule,
    DropdownModule,
    ReactiveFormsModule
  ],
  template: `
    <p>
      demo1 works!
    </p>

    {{date}} - {{input.value}}

    <p-dropdown
      [options]="cities"
      [(ngModel)]="selectedCity"
      optionLabel="name"
      placeholder="Select a City"
      (onChange)="doSomething($event)"
    />
    
    <p-calendar [formControl]="input"/>

    <button (click)="openDropDown()">OPEN DROPDOWN</button>
  `,
  styles: ``
})
export default class Demo1Component {
  dropdownRef = viewChild(Dropdown)
  date = new Date();
  input = new FormControl(new Date())

  cities: City[] | undefined;

  selectedCity: City | undefined;

  ngOnInit() {
    this.cities = [
      { name: 'New York', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];
  }

  openDropDown() {
    this.dropdownRef()!.show()
  }
  doSomething(ev: DropdownChangeEvent) {
    console.log('change', ev.value)
  }
}
interface City {
  name: string;
  code: string;
}
