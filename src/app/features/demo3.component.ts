import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { ColorPickerModule } from 'primeng/colorpicker';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    CalendarModule,
    ColorPickerModule,
    ReactiveFormsModule,
    JsonPipe
  ],
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="text">
      <p-colorPicker formControlName="color" />
      <p-calendar formControlName="date" />
    </form>

    <pre>valid: {{form.valid | json}}</pre>
    <pre>{{form.value | json}}</pre>
  `,
  styles: ``
})
export default  class Demo3Component {
  fb = inject(FormBuilder)

  form = this.fb.group({
    text: 'fabio',
    color: ['', [Validators.required]],
    date: new Date(),
  })
}
