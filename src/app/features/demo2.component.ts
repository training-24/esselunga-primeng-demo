import { Component, inject, signal } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [
    ToastModule,
    ConfirmDialogModule,
    ButtonModule
  ],
  template: `
    <div class="card flex justify-content-center gap-2">
      <p-toast />
     
      <p-button (click)="confirm1($event)" label="Save" [outlined]="true" />
      <p-button (click)="confirm2($event)" label="Delete" severity="danger" [outlined]="true" />
    </div>

    <p-confirmDialog />
    
    @if(visible()) {
      <div>
        confirmed
      </div>  
    }
    
  `,
  styles: ``,
  providers: [
    ConfirmationService,
    MessageService
  ],
})
export default  class Demo2Component {
  confirmationService = inject(ConfirmationService)
  messageService = inject(MessageService)
  visible = signal(false)

  confirm1(event: Event) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Are you sure that you want to proceed?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      acceptIcon:"none",
      rejectIcon:"none",
      rejectButtonStyleClass:"p-button-text",
      accept: () => {
        console.log('quello che voglio!')
        this.visible.set(true)
        this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted' });
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
      }
    });
  }

  confirm2(event: Event) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      acceptButtonStyleClass:"p-button-danger p-button-text",
      rejectButtonStyleClass:"p-button-text p-button-text",
      acceptIcon:"none",
      rejectIcon:"none",

      accept: () => {
        this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'Record deleted' });
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected' });
      }
    });
  }

}
